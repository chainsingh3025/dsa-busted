package Pattern1;

public class Pattern8 {

    public static void main(String[] args) {
        /*
        *   pattern
        *   _ _ _ *
        *   _ _ * *
        *   _ * * *
        *   * * * *
        *
        * */

        int row , col , n=4;
        row =1;
        while(row <= 4){
            int space = n - row;
            //print spaces as n-row
            while(space != 0){
                System.out.print(" ");
                space--;
            }
            col =1;
            while(col <= row){
                System.out.print("*");
                col++;
            }
            row++;
            System.out.println();
        }

        /*
         *   pattern
         *   * * * *
         *   _ * * *
         *   _ _ * *
         *   _ _ _ *
         *
         * */
        row =1;
        while(row <= n){
            // print spaces
            int space = row -1;
            while (space != 0){
                System.out.print(" ");
                space--;
            }
            col =1;
            while(col <= n - row +1){
                System.out.print("*");
                col++;
            }
            System.out.println();
            row++;
        }
    }

}
