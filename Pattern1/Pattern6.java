package Pattern1;

public class Pattern6 {

    public static void main(String[] args) {
         /*
            this pattern code
            A
            B B
            C C C
          */
        int n=4;
        int row,col;
        row=1;
        while(row <= n){
            col =1;
            char c ='A';
            while(col <= row){
                System.out.print((char)(c+row-1));
                col++;
            }
            row++;
            System.out.println();
        }
    }

}
