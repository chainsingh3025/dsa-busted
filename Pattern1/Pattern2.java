package Pattern1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Pattern2 {
    public static void main(String[] args) throws IOException{
        BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
        
        int n = bis.read();
        System.out.println(n);

/* 
            this pattern code

            1
            23
            345
            45673
without extra variable
            */

        int row =1,col;
        while(row <= n ){
            col = 1;
            while(col <= row){
                System.out.print(row+col-1);
                col++;
            }
            System.out.println();
            row++;
        }

        

    }
}
