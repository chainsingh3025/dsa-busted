package Pattern1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class Pattern1 {
    public static void main(String[] args) throws IOException{
         /* 
            this pattern code

            * * * *
            * * *
            * * 
            * 

            */

            BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
        
            int n = bis.read();
            System.out.println(n);
        int row , col;

        row=1;
        while ( row <=4 ){
            col = n - row +1;
            while(col > 0){
                System.out.print("*");
                col--;    
            }
            row++;
            System.out.println();
        }
    }
}
