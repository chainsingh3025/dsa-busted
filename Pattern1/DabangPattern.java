package Pattern1;

public class DabangPattern {

    /*
     *   pattern
     *   1 2 3 4 5 5 4 3 2 1
     *   1 2 3 4 * * 4 3 2 1
     *   1 2 3 * * * * 3 2 1
     *   1 2 * * * * * * 2 1
     *   1 * * * * * * * * 1
     *
     *
     * */
    public static void main(String[] args) {

        int row = 1,n=5,col , temp;
        while(row <= n){
            //print oth triangle
            col =1;
            while(col <= n - row + 1){
                System.out.print(col);
                col++;
            }
            //print 1st triangle
            temp = row - 1;
            while(temp > 0){
                System.out.print("*");
                temp--;
            }
            //print 2nd triangle
            temp = row - 1;
            while(temp > 0){
                System.out.print("*");
                temp--;
            }
            //print 3rd triangle
            col = n -row +1;
            while(col > 0){
                System.out.print(col);
                col--;
            }

            System.out.println();
            row++;

        }
    }
}
