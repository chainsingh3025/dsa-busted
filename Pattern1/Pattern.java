package Pattern1;
public class Pattern {
        private static int row = 1;

        public static void main(String[] args) {
            /* 
            this pattern code

            *
            * *
            * * *
            * * * *

            */
            System.out.println("Pattern 1");
            int col,n=4;row=1;
            while(row <= n){
                col=1;
                while(col<=row){
                    System.out.print("*");
                    col++;
                }
                row++;
                System.out.println("\n");
            }

            
            /* 
            this pattern code


            1
            22
            333
            4444

            

            */
            System.out.println("Pattern 2");
            row=1;
            while(row <= n){
                col=1;
                while(col<=row){
                    System.out.print(row);
                    col++;
                }
                row++;
                System.out.println("\n");
            }



            /* 
            this pattern code

            

            1
            12
            123
            1234

            */
            System.out.println("Pattern 3");
            row=1;
            while(row <= n){
                col=1;
                while(col<=row){
                    System.out.print(col);
                    col++;
                }
                row++;
                System.out.println("\n");
            }

            /* 
            this pattern code

            

            1
            23
            456
            789 10

            */
            System.out.println("Pattern 3");
            row=1;
            int count = 1;
            while(row <= n){
                col=1;
                while(col<=row){
                    System.out.print(count);
                    col++;
                    count++;
                }
                row++;
                System.out.println("\n");
            }

            /* 
            this pattern code

            

            1
            23
            345
            4567

            */
            System.out.println("Pattern 3");
            row=1;
            
            while(row <= n){
                col=1;
                count = row;
                while(col<=row){
                    System.out.print(count);
                    col++;
                    count++;
                }
                row++;
                System.out.println("\n");
            }

            

        }
}
