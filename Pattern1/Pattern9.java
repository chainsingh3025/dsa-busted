package Pattern1;

public class Pattern9 {

    public static void main(String[] args) {
        /*
         *   pattern
         *   _ _ _ 1 _ _ _
         *   _ _ 1 2 1 _ _
         *   _ 1 2 3 2 1 _
         *   1 2 3 4 3 2 1
         *
         * */

        int row , col , n=5;
        row =1;
        while(row <= n){
            int space = n - row;
            while(space > 0){
                System.out.print(" ");
                space--;
            }
            col = 1;
            while(col <= row){
                System.out.print(col);
                col++;
            }
            int k = row -1;
            while(k > 0){
                System.out.print(k);
                k--;
            }
            System.out.println();
            row++;
        }
    }
}
