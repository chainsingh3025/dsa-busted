package Pattern1;

public class Pattern5 {
    public static void main(String[] args) {
        /* 
            this pattern code

            A B C
            B C D
            C D E
            

            */

        int n = 3;
        int row =1;
        int col;
        while(row <= n){
            col =1 ;
            char c = 'A';
            while(col <= n){
                System.out.print((char)(c+row+col-2));
                col++;
            }
            System.out.println();
            row++;
        }
    }
}
