package Pattern1;

public class Pattern7 {

    public static void main(String[] args) {
        /*
            this pattern code
            A
            B C
            D E F
            G H I J
          */
        int n=4;
        int row,col;
        row=1;
        char c = 'A';
        while(row <= n){
            col =1;
            while(col <= row){
                System.out.print((char)c);
                col++;
                c++;
            }
            row++;
            System.out.println();
        }
    }
}
