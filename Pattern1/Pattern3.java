package Pattern1;
public class Pattern3 {
    public static void main(String[] args) {
        /* 
            this pattern code

            A A A
            B B B
            C C C
            

            */

        int n = 3;
        int row =1;
        int col;
        while(row <= n){
            col =1 ;
            char c = 'A';
            while(col <= n){
                System.out.print((char)(c+(row-1)));
                col++;
            }
            System.out.println();
            row++;
        }
    }
}
