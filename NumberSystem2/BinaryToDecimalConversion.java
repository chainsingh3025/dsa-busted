package NumberSystem2;

public class BinaryToDecimalConversion {

    public static void main(String[] args) {
        int binary = 1011,i=0,ans = 0;
        while(binary > 0){
            int last_digit = binary % 10;  // binary & 1
            binary =binary /10;
            if(last_digit == 1)
                ans = ans + (int) (Math.pow(2,i)*last_digit);
            i++;
        }
        System.out.println(ans);
    }

}
