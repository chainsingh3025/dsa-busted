package NumberSystem2;

public class DecimalToBinaryConversion1 {

    /*
    decimal to binary conversion
    --> to convert decimal to binary
     we divide decimal number by 2 , store/ print the reminder as either it could be
     0 or 1 , now number = number / 2 , do this until number is not equal zero.
     you will see output is in reverse order, so atlast we reverse the reminder output
     */
    public static void main(String[] args) {
        int num = 11;
        getBinaryNumberByApproach1(num);
        int result = getBinaryNumberByApproach2(num);
        System.out.println("Decimal :"+num+ " to Binary : "+result);
    }

    private static int getBinaryNumberByApproach2(int num) {
        // in this approach will not use string builder
        int sum = 0,temp,i=0;

        while (num!=0){
            System.out.println(num%2);
            temp = num %2;   // here we get last bit of number
            // both num %2 , num&1 give last bit of number as any bit & with 1 gives
            // 1 when both are bits are 1
            sum = (int)(temp * Math.pow(10,i) ) + sum;
            i++;
            num>>=1;   // right by 2 is equal to divide by 2
        }
        return sum;
    }

    // this is first approach and basic one
    private static int getBinaryNumberByApproach1(int num) {
        int sum = 0,temp,i=0;
        StringBuilder s = new StringBuilder();
        while (num!=0){
            System.out.println(num%2);
            temp = num %2;
            s.append(temp);
            num>>=1;   // right by 2 is equal to divide by 2
        }
        System.out.println(s);
        // now reverse s.toString() value
        System.out.println("First approach :"+s.reverse());
        return sum;

    }

}
