package Array;

public class RainWaterTrappedProblem {

    public static void main(String[] args) {
        // given builds with different heights and they are consecutive align with each other
        // heavy rain is happening in city . find the water logded in builds

        int builds[] = {
            3, 4, 6, 2, 4, 9, 1, 4, 5, 8, 4, 4
        };

        // this is question of prefix max and suffix max which is preprocessing

        int waterLodged = caluclateWaterLodged(builds, builds.length);
        // this approach takes o(n) time complexity and o(2n) space complexity
        // first prefix max and suffix max takes o(n) TC
        // buildings iterate also takes o(n) tc
        // here we take additional m/m as have used 2 array of n size
        System.out.println("water lodged : " + waterLodged);

        // slightly optimized approach
        waterLodged = caluclateWaterLodgedOptimized1(builds, builds.length);

    }

    private static int caluclateWaterLodgedOptimized1(int[] builds, int length) {
        int sum = 0;
        /*
        As we know , prefix sum array and buildings loop iterate from left to right
        so we will compute prefix max calculation along the iteration of buildings
         */
        // we calculate suffix max here first
        int suffixMax[] = new int[length];
        suffixMax[length - 1] = builds[length - 1];
        for (int i = length - 2; i >= 0; i--) {
            if (builds[i] > suffixMax[i + 1]) {
                suffixMax[i] = builds[i];
            } else {
                suffixMax[i] = suffixMax[i+1];
            }
        }
        printArray(suffixMax);

        int prefixMax =builds[0];
        int height;
        for(int i=0;i<length;i++){
            // in this same loop we will calculate prefix max and sum of water lodged
            height = builds[i];
            prefixMax = prefixMax;

        }
        return sum;
    }

    private static int caluclateWaterLodged(int[] builds, int length) {
        int sum = 0;
        /*
            As we know ,In an building water can lodged if only , its left and right
            buildings are greater then itself, and upon those both building water will be
            lodged only at height of min (left building max height , right buildding max height only)
            so for calcuation left , right building max height , we will preprocess the these values
            , as these are nothing but prefix max and suffix max on array.
            So first step is to calculate prefix max and suffix max on building
         */
        int leftMaxHeights[] = new int[length];
        leftMaxHeights[0] = builds[0];
        for (int i = 1; i < length; ++i) {
            if (builds[i] < leftMaxHeights[i - 1]) {
                leftMaxHeights[i] = leftMaxHeights[i - 1];
            } else {
                leftMaxHeights[i] = builds[i];
            }
        }
        printArray(leftMaxHeights);
        // index    { 0,1,2,3,4,5,6,7,8,9,10,11}
        // builds   { 3,4,6,2,4,9,1,4,5,8,4,4 }
        // prefixmax{ 3,4,6,6,6,9,9,9,9,9,9,9 }
        // suffixMax{ 9,9,9,9,9,9,8,8,8,8,4,4 }
        // for prefix we iterate from left to right and suffix max we iterate from right to left
        int rightMaxHeights[] = new int[length];
        rightMaxHeights[length - 1] = builds[length - 1];
        for (int i = length - 2; i >= 0; --i) {
            if (builds[i] < rightMaxHeights[i + 1]) {
                rightMaxHeights[i] = rightMaxHeights[i + 1];
            } else {
                rightMaxHeights[i] = builds[i];
            }
        }
        printArray(rightMaxHeights);

        // now we will iterate buildings and for each building we will look for
        // max height

        for (int i = 0; i < length; i++) {
            int height = builds[i];
            int rightMaxHeight = rightMaxHeights[i];
            int leftMaxHeight = leftMaxHeights[i];
            int support = Math.min(leftMaxHeight, rightMaxHeight);
            if (support > height) {
                sum += support - height;
            }
        }

        return sum;
    }

    private static void printArray(int[] builds) {
        for (int i = 0; i < builds.length; i++) {
            System.out.print(builds[i]);
        }
        System.out.println();

    }

}
