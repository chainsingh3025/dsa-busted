package Array;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

public class FileSize {

    public static void main(String[] args) {
        String path = "/home/raider/Pictures/resume.png";
//6.2 = 8400000
        String s = new String("=");
        int length1 = Base64.decode(s).length;

        Path path1 = Paths.get(path);

        try {

            // size of a file (in bytes)
            long bytes = Files.size(path1);
            System.out.println(String.format("%,d bytes", bytes));
            System.out.println(String.format("%,d kilobytes", bytes / 1024));
            System.out.println(String.format("%,d megabytes", (float)(bytes / 1024)/1024));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
