package Array;

public class GoodPairApproach1 {

  /* Question
   * Given an array A and an integer B.
   * A pair(i, j) in the array is a good pair if i != j and (A[i] + A[j] == B).
   * Check if any good pair exist or not.
   * */

  /*
  *  Now my approach
  * For every i run a loop of j and check if A[i]+A[j]==B or not. Also, check if i!=j.
  * */

  public static void main(String[] args) {
    int[] A = { 1, 4,2,3,4,7};
    int b = 11;
    System.out.println(solve(A,b));
  }

  public static int solve(int[] A, int B) {
    for(int i=0;i<A.length-1;i++){
      for(int j=i+1;j<A.length;j++){
        int temp =A[i]+A[j];
        if(temp == B){
          return 1;
        }
      }
    }
    return 0;
  }
}
