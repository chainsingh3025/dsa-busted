package Array;

import java.util.Scanner;

public class RotateArrayByXelement_2approach {

  /*
   *  this approach is much simpler than previous one.
   *   A = [1,2,3,4]
   *  b = 2
   *  1. first reverse whole array
   *    [4,3,2,1]
   *  2. reverse sub array from [0,b] exclude b value , b =2
   *  [3,4,2,1]
   *  3. reverse sub array from [b+1,A.length] exclude b value , b =2
   *  [3,4,1,2]
   *
   * */
  public static void main(String[] args) {
    // YOUR CODE GOES HERE
    // Please take input and print output to standard input/output (stdin/stdout)
    // DO NOT USE ARGUMENTS FOR INPUTS
    // E.g. 'Scanner' for input & 'System.out' for output
    Scanner scanner = new Scanner(System.in);
    System.out.print("Enter the number of elements you want to store: ");
    int a = scanner.nextInt();
    int[] arr = new int[a];
    int i = 0;
    System.out.println("Enter the elements of the array: ");
    while (i < a) {
      arr[i] = scanner.nextInt();
      i++;
    }
    System.out.println("Enter the rotate of the array: ");
    int rotate = scanner.nextInt()%arr.length;

//      int[] arr = {1,2,2};
//      int rotate =3;

    // step 1 : for explanation see above comments
    reverse(arr,0,arr.length-1);
    // step 2
    reverse(arr,0,rotate-1);
    // step 3
    reverse(arr,rotate,arr.length-1);

    //print array
    for(int k=0;k<arr.length;k++){
      System.out.print(arr[k]+" ");
    }





  }

  private static void reverse(int[] arr, int left, int right) {

    while(left < right){
      int temp = arr[left];
      arr[left] = arr[right];
      arr[right] = temp;
      left++;right--;
    }
  }

}
