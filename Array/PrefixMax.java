package Array;

import java.util.Arrays;

public class PrefixMax {

    public static void main(String[] args) {
        int arr[] = {
            5, 3, 6, 3, 4, 7, 2, 4, 6
          };
        System.out.println("arr before : ");
        printArr(arr);
        arr = calculatePrefixMax(arr,arr.length);
        System.out.println("arr after : ");
        printArr(arr);
    }

    private static void printArr(int[] arr) {
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]);
        }
    }

    private static int[] calculatePrefixMax(int[] arr, int length) {
        int max = arr[0];
        for(int i =0;i<length;i++){
            if(max > arr[i]){
                arr[i] = max;
            }else {
                max = arr[i];
            }
        }
        return arr;

    }

}
