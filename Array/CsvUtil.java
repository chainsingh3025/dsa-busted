package Array;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CsvUtil {

    public static boolean matches(String value) {
        if (value == null) {
            return false;
        } else {
            String regex = ".a";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(value);
            return matcher.matches();
        }
    }

    public static void main(String[] args) {

        Pattern regex = Pattern.compile("[@+\\-=].+?");
        Matcher matcher = regex.matcher("-abc singh ");

        System.out.println("---------------" + matcher.find());
        System.out.println("---------------" + matcher.matches());
    }

}
