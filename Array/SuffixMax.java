package Array;

public class SuffixMax {

    public static void main(String[] args) {
        int arr[] = {
            5, 3, 6, 3, 4, 7, 2, 4, 6
        };
        System.out.println("arr before : ");
        printArr(arr);
        arr = calculateSuffixMax(arr,arr.length);
        System.out.println("arr after : ");
        printArr(arr);
    }

    private static int[] calculateSuffixMax(int[] arr, int length) {
        int max = arr[length-1];
        for(int i = length -2 ;i >=0;i--){
            if(arr[i] > max){
                max =arr[i];
            }
            else{
                arr[i] = max;
            }
        }
        return arr;
    }

    private static void printArr(int[] arr) {
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]);
        }
    }
}
