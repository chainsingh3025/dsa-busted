package Array;

import java.util.Scanner;

public class RotateArrayByXelement {

    public static void main(String[] args) {
      // YOUR CODE GOES HERE
      // Please take input and print output to standard input/output (stdin/stdout)
      // DO NOT USE ARGUMENTS FOR INPUTS
      // E.g. 'Scanner' for input & 'System.out' for output
      Scanner scanner = new Scanner(System.in);
      System.out.print("Enter the number of elements you want to store: ");
      int a = scanner.nextInt();
      int[] arr = new int[a];
      int i = 0;
      System.out.println("Enter the elements of the array: ");
      while (i < a) {
        arr[i] = scanner.nextInt();
        i++;
      }
      System.out.println("Enter the rotate of the array: ");
      int rotate = scanner.nextInt();

//      int[] arr = {1,2,2};
//      int rotate =3;

      rotate(arr, rotate);

    }
    public static void rotate(int[] arr, int rotate) {
      rotate = rotate % arr.length;

      // creating temp array and storing rotate values in it
      int[] temp = new int[rotate];
      int i = arr.length - rotate;
      int length = arr.length;
      int j = 0;
      while (i < length) {
        temp[j] = arr[i];
        i++;
        j++;
      }

      // shift element to their respective postion
      int left = length - rotate - 1;
      int right = length - 1;
      while (left >= 0) {
        arr[right] = arr[left];
        left--;
        right--;
      }

      // placing temp array in original array
      left++;
      int x = temp.length;
      while (x > 0) {
        arr[left] = temp[left];
        left++;
        x--;
      }

      // print array
      for (int k = 0; k < length; k++) {
        System.out.print(arr[k] + " ");
      }
    }

}
